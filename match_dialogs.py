import sys
from collections import Counter
from itertools import groupby
import re
from random import randint
from math import log10
from nltk.corpus import stopwords
from nltk.tokenize import wordpunct_tokenize


stop_words = set(stopwords.words('english'))
stop_words.update(['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']',
                   '{', '}'])


def clean_word(word):
    """Removes certain characters from a word."""
    removable_characters = "[?.,\'\"!: ]"
    word = re.sub(removable_characters, '', word)
    return word.lower()


def tokenizer(sentence):
    """Creates list of words from a sentence."""
    list_of_words = [i.lower() for i in wordpunct_tokenize(sentence)
                     if i.lower() not in stop_words]
    return list_of_words


def read_file(file_name, separator=" +++$+++ "):
    """Reads and splits each line using separator"""
    with open(file_name, 'r') as f:
        data = [i.split(separator, 1) for i in list(f)]
        return data


def get_priors(data):
    """Determines the probabilities of each category."""
    priors = {}
    categories = [i[0] for i in data]
    categories_count = Counter(categories).most_common()
    categories_unique = [c[0] for c in categories_count]
    categories_probability = [float(c[1])/len(data) for c in categories_count]
    for i in range(len(categories_unique)):
        priors[categories_unique[i]] = categories_probability[i]
    return priors


def combine_words_by_category(data):
    """It collects all the words belonging to one category and puts them
    to one list. Therefore by this modification, the probability of
    each category becomes same and constant and equals to 1/(# categories) """
    categories_words = {}
    for key, group in groupby(data, lambda x: x[0]):
        sentence = ""
        for i in group:
            sentence += " {}".format(i[1])
        categories_words[key] = tokenizer(sentence)
    return categories_words


def get_likelihoods(data):
    """Determines the probability of all words of all particular
    categories given that category."""
    categories_words = combine_words_by_category(data)
    category_word_probability = {}
    for category in categories_words.keys():
        category_word_probability[category] = {}
        words_count = Counter(categories_words[category])
        unique_words = [c[0] for c in words_count.most_common()]
        total_words = len(categories_words[category])
        for word in unique_words:
            category_word_probability[category][word] = \
                float(words_count[word])/total_words
    return category_word_probability


def get_words_probability(data):
    """Determines the probability of word in a data."""
    categories_words = combine_words_by_category(data)
    words = []
    words_probability = {}
    for i in categories_words.keys():
        words.extend(categories_words[i])
    words_count = Counter(words)
    unique_words = [c[0] for c in words_count.most_common()]
    total_words = len(words)
    for word in unique_words:
        words_probability[word] = float(words_count[word])/total_words
    return words_probability


def update_posterior(p, word_likelihood):
    """A function that updates the posterior."""
    # p *= category_probability*word_likelihood
    p += log10(word_likelihood)
    return p


def get_posteriors(sentence, priors, likelihoods, words_probability):
    """Determines the probability of a given sentence
    belonging to each category."""
    categories_probability = {}
    for category in priors.keys():
        p = log10(priors[category])
        for word in tokenizer(sentence):
            try:
                word_likelihood = likelihoods[category][word]
            except KeyError:
                word_likelihood = 1e-10
            p = update_posterior(p, word_likelihood)
        categories_probability[category] = p
    return categories_probability


def get_category(sentence, priors, likelihoods, words_probability):
    """Finds the maximum of the posterior of all categories."""
    categories_probability = get_posteriors(sentence, priors, likelihoods,
                                            words_probability)
    categories = priors.keys()
    p = categories_probability[categories[0]]
    category = ""
    for c in categories:
        if categories_probability[c] > p:
            p = categories_probability[c]
            category = c
    return category, p


def calculate_efficiency(missing_data, priors, likelihoods, words_probability):
    """Outputs the results in a file and prints out efficiency of the model.
    Also compares with random prediction of senetence."""
    correct = 0
    random_correct = 0
    n = 0
    categories = priors.keys()
    total_data_size = len(missing_data)
    with open('test_missing_with_predictions.txt', 'w') as output:
        for s in missing_data:
            n += 1
            category, p = get_category(s[1], priors, likelihoods,
                                       words_probability)
            output.write("%s ---+--- %s" % (category, s[1]))
            # if category == s[0]:
            #     correct += 1
            #
            # if categories[randint(0, len(categories)-1)] == s[0]:
            #     random_correct += 1
            # print "Learning efficiency: {} % | Random efficiency: {} %".format(
            #         round(float(correct)/n*100, 2),
            #         round(float(random_correct)/n*100, 2))


if __name__ == '__main__':
    """
    This script should be called as 
    python match_dialogs.py path/to/test_dialogs.txt path/to/test_missing.txt
    and write the predicted conversation numbers for all missing lines to a
    file named test_missing_with_predictions.txt
    """
    # if called with file names, load data from there else load from default
    # location / output an error
    if len(sys.argv) <= 2:
        raise ValueError("please call this script with `python "
                         "match_dialogs.py "
                         "path/to/test_dialogs.txt path/to/test_missing.txt`")
    dialogs_data, missing_data = read_file(sys.argv[1]), read_file(sys.argv[2])

    priors = get_priors(dialogs_data)
    likelihoods = get_likelihoods(dialogs_data)
    words_probability = get_words_probability(dialogs_data)

    with open('priors.txt', 'w') as f:
        categories = priors.keys()
        for c in categories:
            f.write("%s -- %s\n" % (c, priors[c]))
    with open('words_probability.txt', 'w') as f:
        words = words_probability.keys()
        for w in words:
            f.write("%s -- %s\n" % (w, words_probability[w]))
    calculate_efficiency(missing_data, priors, likelihoods, words_probability)
